
# == MapR New Node Setup Script Start == #

# Rescan for the new disks
echo "- - -" > /sys/class/scsi_host/host0/scan
echo "- - -" > /sys/class/scsi_host/host1/scan
echo "- - -" > /sys/class/scsi_host/host2/scan

# Create the physical volumes
pvcreate /dev/sdb
pvcreate /dev/sdc

# Extend the root file system to accomodate the MapR install
vgextend centos /dev/sdb
lvextend -l 100%FREE /dev/centos/root
xfs_growfs /

# == MapR New Node Setup Script End == #

 =========================================

# == MapR Install Script Start == #

# Install WGET and download MapR
yum -y install wget
wget http://package.mapr.com/releases/installer/mapr-setup.sh -P /tmp

# Run the MapR Setup with default values
bash /tmp/mapr-setup.sh --yes

# Update MapR
bash /opt/mapr/installer/bin/mapr-setup.sh --yes
yum -y update mapr-installer mapr-installer-definitions

# Run the installer to add the nodes to the cluster
su mapr -c "cd /opt/mapr/installer/bin/;./mapr-installer-cli install --no_check_certificate --verbose --force --template /tmp/config.yaml"

# == MapR Install Script End == #

 =========================================

# == MapR New Node Script Start == #

# Run the installer to add the nodes to the cluster
su mapr -c "cd /opt/mapr/installer/bin/;./mapr-installer-cli install --no_check_certificate --verbose --force --template /tmp/newnodeconfig.yaml"

# == MapR New Node Script End == #