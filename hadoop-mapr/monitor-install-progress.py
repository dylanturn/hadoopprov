import os
import requests
import time

requestSession = requests.Session()

apiBase = "https://chi01-lab-ap142.lab.transunion.com:9443/api"
apiLogin = "{}/login".format(apiBase)
apiProcess = "{}/process".format(apiBase)

requestResponse = requestSession.get(apiLogin,verify=False,headers={'Content-Type':'application/x-www-form-urlencoded'})
data = {'username':'mapr', 'password':'mapr'}
requestResponse = requestSession.post(apiLogin,verify=False,headers={'Content-Type':'application/x-www-form-urlencoded'}, data=data)
requestResponse = requestSession.get(apiProcess,verify=False,headers={'Content-Type':'application/json'})

print(" ")
print("Process Status:     {}".format(requestResponse.json()['status']))
print("Process State:      {}".format(requestResponse.json()['state']))
print("Process Percentage: {}".format(requestResponse.json()['completion']))

installing = True
while (installing == True):
    requestResponse = requestSession.get(apiProcess,verify=False,headers={'Content-Type':'application/json'})
    print("Status: {} | State: {} | Percentage: {}".format(requestResponse.json()['status'], requestResponse.json()['state'], requestResponse.json()['completion']))
    if requestResponse.json()['state'] not in 'INSTALLING':
        print("it's done!'")
        print(requestResponse.json()['state'])
        installing = False
    time.sleep(5)