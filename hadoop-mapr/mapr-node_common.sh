

# Packages to install on all cluster nodes: mapr-fileserver

echo '[maprtech]
name=MapR Technologies
baseurl=http://package.mapr.com/releases/<version>/redhat/
enabled=1
gpgcheck=0
protect=1

[maprecosystem]
name=MapR Technologies
baseurl=http://package.mapr.com/releases/MEP/MEP-<version>/redhat
enabled=1
gpgcheck=0
protect=1' >> /etc/yum.repos.d/maprtech.repo

# Need to make sure root gets extended. an extra 10 GB should do fine.
# /opt/mapr/server/configure.sh -N dylanhadoop.lab.transunion.com -c -C chi01-lab-ap078.lab.transunion.com
systemctl disable firewalld
rpm --import http://package.mapr.com/releases/pub/maprgpg.key
yum -y install wget java-1.7.0-openjdk-devel mapr-fileserver mapr-tasktracker