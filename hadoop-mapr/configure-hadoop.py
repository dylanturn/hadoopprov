import os
import requests
import thread
import time
import urllib2
import string
from utilities.models import ConnectionInfo
from infrastructure.models import CustomField
from orders.models import CustomFieldValue

def configureCluster(masterServer):
    # Execute the cluster reconfiguration routine
    masterServer.execute_script(script_contents='su mapr -c "cd /opt/mapr/installer/bin/;./mapr-installer-cli install --no_check_certificate --verbose --force --template /root/clusterconfig.yaml"')

def run(job, logger=None):

    if not logger:
        logger = get_thread_logger(__name__)

    if job.status == 'FAILURE':
        return "", "", ""

    masterServer = ""

# {maprCoreVersion}
    maprCoreVersion = "5.2.0"
# {licenseType}
    maprLicenseType = "M3"
# {mepVersion}
    maprMepVersion = "2.0"

# {clusterName}
    clusterName = job.service_set.first().attributes.filter(field__name="mapr_cluster_name").first().value
# {clusterHostList}
    clusterHostList = ""

# {hostSshUsername}
    hostSshUsername = "root"
# {hostSshPassword}
    hostSshPassword = "password"
# {hostSshPort}
    hostSshPort = "22"
# {diskList}
    hostDiskList = "    - {}".format("/dev/sdc")

# {hiveDbHost}
    hiveDbHost = "10.210.26.56"
# {hiveSchemaName}
    hiveSchemaName = "DylanHiveDB"
# {hiveUsername}
    hiveUsername = "34852_admin"
# {hivePassword}
    hivePassword = "password"

    groupControl = ""
    groupMaster = ""
    groupMultiMaster = ""
    groupData = ""
    groupDefault = ""

    if clusterName is None:
        return "FAILURE", "ClusterName Null!", ""

    for server in job.service_set.first().server_set.all():
        
        groupDefault += "    - {}.lab.transunion.com\n".format(server.hostname)
        clusterHostList = groupDefault

        if server.service_item.name == "Control Node":
            groupControl += "    - {}.lab.transunion.com\n".format(server.hostname)

        elif server.service_item.name == "Master Node":
            groupMaster += "    - {}.lab.transunion.com\n".format(server.hostname)
            masterServer = server

        elif server.service_item.name == "Multi_Master Node":
            groupMultiMaster += "    - {}.lab.transunion.com\n".format(server.hostname)

        elif server.service_item.name == "Data Node":
            groupData += "    - {}.lab.transunion.com\n".format(server.hostname)
        
    url = 'https://raw.githubusercontent.com/dylanturn/hadoopprov/master/hadoop-mapr/hadoopsetup.yaml'
    configTemplate = urllib2.urlopen(url).read()

    configTemplate = configTemplate.replace("{maprCoreVersion}",maprCoreVersion)
    configTemplate = configTemplate.replace("{licenseType}",maprLicenseType)
    configTemplate = configTemplate.replace("{mepVersion}",maprMepVersion)

    configTemplate = configTemplate.replace("{clusterName}",clusterName)

    configTemplate = configTemplate.replace("{clusterHostList}",clusterHostList)
    configTemplate = configTemplate.replace("{hostSshUsername}",hostSshUsername)
    configTemplate = configTemplate.replace("{hostSshPassword}",hostSshPassword)
    configTemplate = configTemplate.replace("{hostSshPort}",hostSshPort)

    configTemplate = configTemplate.replace("{diskList}",hostDiskList)
    
    configTemplate = configTemplate.replace("{hiveSchemaName}",hiveSchemaName)
    configTemplate = configTemplate.replace("{hiveUsername}",hiveUsername)
    configTemplate = configTemplate.replace("{hivePassword}",hivePassword)
    configTemplate = configTemplate.replace("{hiveDbHost}",hiveDbHost)

    configTemplate = configTemplate.replace("{groupDefault}",groupDefault)
    configTemplate = configTemplate.replace("{groupMaster}",groupMaster)
    configTemplate = configTemplate.replace("{groupMultiMaster}",groupMultiMaster)
    configTemplate = configTemplate.replace("{groupData}",groupData)
    configTemplate = configTemplate.replace("{groupControl}",groupControl)

    job.set_progress(configTemplate)

    if masterServer is None:
        return "FAILURE","Missing master server!",""
    
    job.set_progress("Writing config yaml to Master server: {}".format(masterServer))

    # Create the config yaml file.
    masterServer.execute_script(script_contents="echo '{}' > /root/clusterconfig.yaml".format(configTemplate))

    try:
        # Execute the cluster reconfiguration routine
        thread.start_new_thread( configureCluster(masterServer) )
    except:
        job.set_progress("Failed to execute installation!")
    
    requestSession = requests.Session()
    apiBase = "https://{}.lab.transunion.com:9443/api".format(masterServer.hostname)
    apiLogin = "{}/login".format(apiBase)
    apiProcess = "{}/process".format(apiBase)

    requestResponse = requestSession.get(apiLogin,verify=False,headers={'Content-Type':'application/x-www-form-urlencoded'})
    data = {'username':'mapr', 'password':'mapr'}
    requestResponse = requestSession.post(apiLogin,verify=False,headers={'Content-Type':'application/x-www-form-urlencoded'}, data=data)

    requestResponse = requestSession.get(apiProcess,verify=False,headers={'Content-Type':'application/json'})

    installing = True
    while (installing == True):
        requestResponse = requestSession.get(apiProcess,verify=False,headers={'Content-Type':'application/json'})
        job.set_progress("Status: {} | State: {} | Percentage: {}".format(requestResponse.json()['status'], requestResponse.json()['state'], requestResponse.json()['completion']))
        if requestResponse.json()['state'].lower() not in "installing":
            installing = False
        time.sleep(5)

    return "", "", ""